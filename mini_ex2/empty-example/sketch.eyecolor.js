function setup() {
  createCanvas(1000, 600);
}

function draw() {
  background(220);

  push();

  fill(255, 255,255 );
  strokeWeight(10);
  stroke(0,0,0)
  ellipse(500, 300, 650, 500);
  pop();


  push();
  fill(0, 0, 0);
  strokeWeight(9);
  pop();

  push();
  fill(0, 0, 0);
  strokeWeight(90);
  frameRate(5);
  stroke(random(30), random(50), random(100));
  ellipse(500, 300, 300, 300);
}
